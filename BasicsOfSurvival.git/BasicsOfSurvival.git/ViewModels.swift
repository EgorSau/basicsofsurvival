//
//  ViewModels.swift
//  BasicsOfSurvival.git
//
//  Created by Egor SAUSHKIN on 23.01.2023.
//

protocol ViewModelProtocol {
}

protocol Setupable {
    func configure(with viewModel: ViewModelProtocol)
}

struct TextViewModel: ViewModelProtocol {
    var title: String
    var text: String
}

struct PictureViewModel: ViewModelProtocol {
    var imageName: String
}


