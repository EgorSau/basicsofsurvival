//
//  PictureCellView.swift
//  BasicsOfSurvival.git
//
//  Created by Egor SAUSHKIN on 23.01.2023.
//

import UIKit

class PictureCellView: UITableViewCell {
    
    private lazy var picture: UIImageView = {
        let picture = UIImageView()
        picture.contentMode = .scaleAspectFill
        picture.clipsToBounds = true
        picture.layer.cornerRadius = 8
        picture.translatesAutoresizingMaskIntoConstraints = false
        return picture
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        self.contentView.addSubview(picture)
        let topConstraint = self.picture.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20)
        let leadingConstraint = self.picture.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 15)
        let trailingConstraint = self.picture.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -15)
        let bottomConstraint = self.picture.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20)
        let imageViewAspectRatio = self.picture.heightAnchor.constraint(equalTo: self.picture.widthAnchor, multiplier: 0.6)
        
        NSLayoutConstraint.activate([
            topConstraint,
            leadingConstraint,
            trailingConstraint,
            bottomConstraint,
            imageViewAspectRatio
        ])
    }
}

extension PictureCellView: Setupable {
    func configure(with viewModel: ViewModelProtocol) {
        guard let viewModel = viewModel as? PictureViewModel else { return }
        picture.image = UIImage(named: viewModel.imageName)
    }
}
