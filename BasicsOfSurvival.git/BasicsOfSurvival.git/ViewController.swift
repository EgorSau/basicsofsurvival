//
//  ViewController.swift
//  BasicsOfSurvival.git
//
//  Created by Sergei Vikhliaev on 20.01.2023.
//

import UIKit

class ViewController: UIViewController {
    
    var dataSource = DataSource()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(TextCellView.self, forCellReuseIdentifier: "TextCell")
        tableView.register(PictureCellView.self, forCellReuseIdentifier: "PictureCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

  override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
  }

    private func setupView() {
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.tableView)
        
        let topTableConstraint = self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor)
        let leadingTableConstraint = self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let trailingTableConstraint = self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        let bottomTableConstraint = self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        
        NSLayoutConstraint.activate([
            topTableConstraint,
            leadingTableConstraint,
            trailingTableConstraint,
            bottomTableConstraint
        ])
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath) as? TextCellView else {
                return UITableViewCell()
            }
            
            let textModel = TextViewModel(title: dataSource.title, text: dataSource.text)
            cell.configure(with: textModel)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PictureCell", for: indexPath) as? PictureCellView else {
                return UITableViewCell()
            }
            
            let textModel = PictureViewModel(imageName: dataSource.image)
            cell.configure(with: textModel)
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}
