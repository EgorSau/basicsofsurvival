//
//  TextCellView.swift
//  BasicsOfSurvival.git
//
//  Created by Egor SAUSHKIN on 23.01.2023.
//

import UIKit

class TextCellView: UITableViewCell {
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 24)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 100
        label.font = UIFont(name: "Mark Pro", size: 24)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        self.contentView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        
        let topStackConstraint = self.stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10)
        let leadingStackConstraint = self.stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 15)
        let trailingStackConstraint = self.stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -15)
        let bottomStackConstraint = self.stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10)
        
        NSLayoutConstraint.activate([
            topStackConstraint,
            leadingStackConstraint,
            trailingStackConstraint,
            bottomStackConstraint
        ])
    }
}

extension TextCellView: Setupable {
    func configure(with viewModel: ViewModelProtocol) {
        guard let viewModel = viewModel as? TextViewModel else { return }
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.text
    }
}
